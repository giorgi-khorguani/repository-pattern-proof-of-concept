package com.giorgi.repository;

import com.giorgi.entity.Organization;
import com.giorgi.specifications.SqlSpecification;

import java.util.List;

public class OrganizationRepository implements Repository<Organization> {
    @Override
    public void add(Organization item) {
        // tbd
    }

    @Override
    public void remove(Organization item) {
        // tbd
    }

    @Override
    public List<Organization> query(SqlSpecification specification) {
        System.out.println("Querying the repository by specification " + specification.getClass().toString());

        System.out.println(specification.toSqlQuery());

        return null;
    }
}

