package com.giorgi.repository;

import com.giorgi.specifications.SqlSpecification;

import java.util.List;

public interface Repository<T> {
    void add(T item);

    void remove(T item);

    List<T> query(SqlSpecification specification);
}
