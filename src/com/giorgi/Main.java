package com.giorgi;

import com.giorgi.repository.OrganizationRepository;
import com.giorgi.repository.Repository;
import com.giorgi.specifications.CreatedAtSpecification;
import com.giorgi.specifications.SqlSpecification;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        Repository organizationRepository = new OrganizationRepository();
        SqlSpecification createdAtTenYearsAgo = new CreatedAtSpecification(new Date(2008, 11, 12));

        organizationRepository.query(createdAtTenYearsAgo);
    }
}
