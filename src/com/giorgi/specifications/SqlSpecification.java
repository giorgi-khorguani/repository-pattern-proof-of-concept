package com.giorgi.specifications;

public interface SqlSpecification {
    String toSqlQuery();
}