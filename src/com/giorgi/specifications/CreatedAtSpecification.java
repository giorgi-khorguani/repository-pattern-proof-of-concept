package com.giorgi.specifications;

import java.util.Date;

public class CreatedAtSpecification implements SqlSpecification {
    private Date createdAt;

    public CreatedAtSpecification(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toSqlQuery() {
        return "SELECT * FROM organizations where createdDate > " + createdAt.toString();
    }
}
